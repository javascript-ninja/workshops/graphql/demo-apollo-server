const { withFilter } = require('apollo-server');
const data = require('lodash');

const TODO_SUBSCRIBE = 'TODO_SUBSCRIBE';

const Todo = [];

module.exports = {
	Query: {
		todos: (parent, args, ctx, info) => Todo
	},
	Mutation: {
		newTodo: (parent, args, { pubsub }, info) => {
			const { task, complete } = args.data;

			// new Todo
			const added = {
				mutation: 'CREATED',
				node: {
					id: Math.random()
						.toString(36)
						.substr(2, 9),
					task,
					complete
				}
			};

			// Push Todo
			Todo.push(added.node);

			// Publish Insert
			pubsub.publish(TODO_SUBSCRIBE, { todo: added });
			return added;
		},
		editTodo: (parent, args, { pubsub }, info) => {
			const {
				fields: { task, complete },
				where: { id }
			} = args;

			// Find by ID
			const edit = data.find(Todo, { id });

			// update
			if (edit.task) {
				edit.task = task;
			}
			if (edit.complete) {
				edit.complete = complete;
			}

			const updated = {
				mutation: 'UPDATED',
				node: edit
			};

			// Publish Update
			pubsub.publish(TODO_SUBSCRIBE, { todo: updated });
			console.log('edit', edit);
			return new Promise(ok => setTimeout(ok, 5000)).then(() => updated);
		},
		removeTodo: (parent, args, { pubsub }, info) => {
			const {
				where: { id }
			} = args;

			const previousLength = Todo.length;
			// Find and Remove
			data.remove(Todo, todos => {
				return todos.id === id;
			});

			if (Math.random() < 0.5) {
				throw new Error('bad luck');
			}

			if (previousLength !== Todo.length) {
				return new Promise(ok => setTimeout(ok, 5000)).then(() => [[id]]);
			} else {
				return [];
			}
		}
	},
	Subscription: {
		todo: {
			subscribe: withFilter(
				(parent, args, { pubsub }, info) =>
					pubsub.asyncIterator(TODO_SUBSCRIBE),
				(payload, variables) => {
					return payload.todo.mutation === variables.mutation;
				}
			)
		}
	}
};
